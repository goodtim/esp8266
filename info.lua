--info.lua

local infoPath = "store.lua";

function infoGetWifiNamePwd()

    file.open(infoPath, "r");
    local all = file.read();
    if (nil == all) then
        file.close();
        return nil, nil;
    end 
    info = cjson.decode(all);
    file.close();
    
    return info.wifiName, info.wifiPwd;
    
end

function infoSetWifiNamePwd(name, pwd) 

    file.open(infoPath, "w");
    local all = file.read();
    if (nil == all) then
        file.write(string.format('{"wifiName":"%s","wifiPwd":"%s"}', name, pwd)); 
    else
        json.wifiName = name;
        json.wifiPwd = pwd;
        file.write(cjson.decode(json));
    end
    
    file.close();

end


