--beep.lua

local beepPin = 8;
local beepOpen = 0;
local beepClose = 1;
local beepTmrId = 0;

function beepInit()

    gpio.mode(beepPin, gpio.OUTPUT, gpio.PULLUP);
    gpio.write(beepPin, gpio.LOW);
    
end

function beepOpenClose(oc)

    if (beepOpen == oc) then
        gpio.write(beepPin, gpio.HIGH);
    else
        gpio.write(beepPin, gpio.LOW);
    end
    
end

function beepOpenSapce(ms)

    local spaceFlag = true;

    tmr.alarm(beepTmrId, ms, 1, function()
        if (spaceFlag) then
            spaceFlag = false;
            gpio.write(beepPin, gpio.HIGH);
        else
            spaceFlag = true;
            gpio.write(beepPin, gpio.LOW);
        end
    end);
    
end

function beepCloseSapce()

    tmr.stop(beepTmrId);
    gpio.write(beepPin, gpio.LOW);
    
end
