--led.lua

led = {};
--引脚定义
local RedPin = 3;
local GreenPin = 1;
local BluePin = 8;
--颜色定义
led.RED = 0;
led.GREEN = 1;
led.BLUE = 2;
led.PURPLE = 3;

--初始化
function led.init() 

    gpio.mode(RedPin, gpio.OUTPUT, gpio.PULLUP);
    gpio.mode(GreenPin, gpio.OUTPUT, gpio.PULLUP);
    gpio.mode(BluePin, gpio.OUTPUT, gpio.PULLUP);
    
end

--打开
function led.open(color)

    close();
    tmr.delay(1000);
    if (RED == color) then
        gpio.write(RedPin, gpio.LOW);
    elseif (GREEN == color) then
        gpio.write(GreenPin, gpio.LOW);
    elseif (BLUE == color) then
        gpio.write(BluePin, gpio.LOW);
    elseif (PURPLE == color) then
       gpio.write(RedPin, gpio.LOW); 
       gpio.write(BluePin, gpio.LOW);
    end
    
end

--关闭
function led.close()

    gpio.write(RedPin, gpio.HIGH);
    gpio.write(GreenPin, gpio.HIGH);
    gpio.write(BluePin, gpio.HIGH);
    
end

--init();
--open(1);
--pio.write(RedPin, gpio.LOW); 
--gpio.write(BluePin, gpio.LOW);



