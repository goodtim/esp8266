--TcpServer.lua

local IP = wifi.sta.getip();
local PORT = 8000;
local TIMEOUT = 60;

tcpServer = net.createServer(net.TCP, TIMEOUT);
tcpServer:listen(PORT, function (socket)

    ip, port = socket:getpeer();
    print("connection ", ip, port);
    socket:on("receive", function (socket, data)
        --echo
        socket:send(data);
    end);
    
    socket:on("disconnection", function (socket, data)
        print("disconnection");
    end);
    
end);

