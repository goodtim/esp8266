--wifi.lua

function wifiInit()

    wifi.stopsmart();
    wifi.setmode(wifi.STATION);
    local name, pwd = infoGetWifiNamePwd();
    print(name, pwd);
    if ("null" == name or "null" == pwd) then
        print("Wifi Information Error");
    elseif ((nil ~= name) and (nil ~= pwd)) then 
        wifi.sta.config(name, pwd);
        wifi.sta.connect();
        local cnt = 0;
        tmr.alarm(0, 1000, 1, function ()
            if ((cnt < 20) and (nil == wifi.sta.getip())) then
                print("Wait Wifi Connect ", cnt);
                cnt = cnt + 1;
            else 
                tmr.stop(0);
                if (cnt >= 20) then
                    log("wifi enter smart link");
                    wifiCfgMode(0);
                end 
                print("Wifi Connect Success [" .. wifi.sta.getip() .. "]");
                --mqttInit();
                --dofile("UdpClient.lua");
                --dofile("TcpServer.lua");
                dofile("beike.lua");
            end
        end);
        return ;
    end
    log("wifi enter smart link");
    wifiCfgMode(0);
end

function wifiCfgMode(mode)

    beepOpenSapce(500);
    -- esptouch 
    if (0 == mode) then
        wifi.startsmart(0, function(ssid, pwd)
            log("esptouch : ", ssid, pwd);
            infoSetWifiNamePwd(ssid, pwd) 
            beepCloseSapce();
        end);
        return ;
    end

    -- airkiss
    if (1 == mode) then
        wifi.startsmart(1, function(ssid, pwd)
            log("airkiss : ", ssid, pwd);
            infoSetWifiNamePwd(ssid, pwd) 
            beepCloseSapce();
        end);
        return ;
    end
end
