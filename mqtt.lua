
local lockWGTopic = "gsd_lock_wg_topic";
local serverTopic = "gsd_lock_server_topic";
local lockWgMac = wifi.sta.getmac();

lockMqtt = mqtt.Client(string.gsub(lockWgMac, ":", ""), 180, "mqtttest/lockwg", "lVwQT4EGlYl7sVEaOmPW7o+Z25QvDyem8xSZesVHtj0=");

lockMqtt:on("offline", function(client)
    print("MQTT Server Offline !!!");
    tmr.stop(1);
    mqttCreateConnect();
end);

lockMqtt:on("message", function(client, topic, data)
    if (nil == data) then
        print("data is nil");
        return ;
    end
    print(topic .. ": " .. data);
end);

local sendCnt = 0;

function mqttCreateConnect() 
    lockMqtt:connect("mqtttest.mqtt.iot.gz.baidubce.com", 1883, 0, function(client)
        print("MQTT Server Connected!!!");
        lockMqtt:subscribe(lockWGTopic, 0, function() end);
            sendCnt = sendCnt + 1;
            lockMqtt:publish(serverTopic, "MQTT Test Send " .. sendCnt, 0, 0);
        end);
    end);
end

function mqttInit() 

    mqttCreateConnect();
    --uart.setup(0, 115200, 8, 0, 1, 0);
    --uart.on("data", function(data)
        --lockMqtt:publish(serverTopic, data, 0, 0);
    --end, 0);
    
    
end